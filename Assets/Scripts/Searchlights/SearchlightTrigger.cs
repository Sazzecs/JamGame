using UnityEngine;
using System.Collections;

public class SearchlightTrigger : MonoBehaviour {

    public bool _isCollision = false;

    private void OnTriggerEnter(Collider other) {
        if (!_isCollision && other.gameObject.layer == LayerMask.NameToLayer("Player")) {
            _isCollision = true;
            StartCoroutine(Stop(0.5f, other.transform.position));
        }
    }

    private IEnumerator Stop(float time, Vector3 playerPos) {
        Animator animator = transform.GetComponentInParent<Animator>();
        if (animator != null) animator.enabled = false;
        Vector3 lastPos = transform.localPosition;
        Vector3 newPos = transform.parent.InverseTransformPoint(playerPos);
        newPos.y = lastPos.y;
        for (float t = 0f; t < time; t += Time.deltaTime) {
            transform.localPosition = Vector3.Lerp(lastPos, newPos, t / time);
            yield return null;
        }
        //transform.localPosition = newPos;
    }
}
