using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public int player = 0;

    public Rigidbody mainObject;

    public float speed;
    public Animator animator;
    public float animatorSpeed = 1f;
    public float rotateTime = 0.25f;
    public float rotateWithUpHandTime = 0.75f;
    public float cameraApproximationTime = 1.5f;
    public float cameraLoseFieldOfView = 30f;

    [HideInInspector] public bool isCameraMove = true;

    private IWinAnimation _winAnim;

    private Vector3 _direction;
    private Rigidbody _rb;

    private Transform _camera;
    private bool _idle = true;
    private Vector3 _previousDirection;
    private bool _canMoving = false;
    private Coroutine _rotateCoroutine = null;

    public bool IsMoving {
        set => _canMoving = value;
    }

    public Rigidbody RigidBody {
        get => _rb;
    }

    private void Start() {
        _rb = gameObject.GetComponent<Rigidbody>();
        _camera = Camera.main.transform;
        _previousDirection = transform.forward;
        animator.speed = animatorSpeed;
        _winAnim = transform.GetComponent<IWinAnimation>();
    }

    private void FixedUpdate() {
        if (!_canMoving) return;
        _direction = Vector3.zero;
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) {
            _direction += Vector3.forward;
        }
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) {
            _direction += Vector3.back;
        }
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) {
            _direction += Vector3.left;
        }
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) {
            _direction += Vector3.right;
        }
        _direction.Normalize();
        if (_direction != Vector3.zero) {
            Rotate(_previousDirection, _direction, 0.25f);
        }
        _rb.velocity = speed * _direction;
        if (_rb.velocity.sqrMagnitude == 0f) {
            Idle();
        } else {
            Run();
        }
        _previousDirection = transform.forward;
    }

    private void LateUpdate() {
        if (!isCameraMove) return;
        _camera.position = new Vector3(transform.position.x, _camera.position.y, _camera.position.z);
    }

    private void OnTriggerEnter(Collider other) {
        if (!_canMoving) return;
        switch (LayerMask.LayerToName(other.gameObject.layer)) {
            case "Searchlight":
                CastleLevelController.Instance.StopCount();
                _canMoving = false;
                _rb.velocity = Vector3.zero;
                StopAllCoroutines();
                StartCoroutine(Lose(_camera));
                break;
            case "Finish":
                CastleLevelController.Instance.StopCount();
                _canMoving = false;
                _rb.velocity = Vector3.zero;
                StopAllCoroutines();
                _winAnim?.PlayWinAnimation(mainObject, animator);
                break;
            default:
                break;
        }
    }

    private void Idle() {
        if (_idle) return;
        animator.SetTrigger("Idle");
        _idle = true;
    }

    private void Run() {
        if (!_idle) return;
        animator.SetTrigger("Run");
        _idle = false;
    }

    private void Rotate(Vector3 from, Vector3 to, float time) {
        if (from == to) return;
        float angle = SignedAngleBetween(from, to, Vector3.up);
        if (_rotateCoroutine != null) StopCoroutine(_rotateCoroutine);
        _rotateCoroutine = StartCoroutine(RotatePlayer(angle, time)); ;
    }

    private IEnumerator RotatePlayer(float angle, float time) {
        for (float t = 0f; t < time; t += Time.deltaTime) {
            float delta = angle / (time / Time.deltaTime);
            transform.Rotate(new Vector3(0, delta, 0));
            yield return null;
        }
    }

    private IEnumerator Lose(Transform camera) {
        mainObject.transform.SetParent(transform.parent);
        mainObject.isKinematic = false;

        Rotate(transform.forward, Vector3.back, rotateWithUpHandTime);
        //float waitingTime = Mathf.Ceil(rotateWithUpHandTime);
        //for (float t = 0f; t < 1f; t += Time.deltaTime) { yield return null; }
        animator.SetTrigger("GiveUp");
        Vector3 currentDirection = camera.forward;
        Vector3 needDirection = (transform.position - camera.position).normalized;
        Camera cameraSettings = camera.GetComponent<Camera>();
        float oldFieldOfView = cameraSettings.fieldOfView;
        for (float t = 0f; t < cameraApproximationTime; t += Time.deltaTime) {
            camera.forward = Vector3.Lerp(currentDirection, needDirection, t / cameraApproximationTime);
            cameraSettings.fieldOfView = Mathf.Lerp(oldFieldOfView, cameraLoseFieldOfView, t / cameraApproximationTime);
            yield return null;
        }
        for (float t = 0f; t < 2f; t += Time.deltaTime) { yield return null; }
        CastleLevelController.Instance.EndGame(false);
    }

    private float SignedAngleBetween(Vector3 a, Vector3 b, Vector3 n) {
        // angle in [0,180]
        float angle = Vector3.Angle(a, b);
        float sign = Mathf.Sign(Vector3.Dot(n, Vector3.Cross(a, b)));

        // angle in [-179,180]
        float signedAngle = angle * sign;

        // angle in [0,360] (not used but included here for completeness)
        //float angle360 =  (signed_angle + 180) % 360;

        return signedAngle;
    }
}
