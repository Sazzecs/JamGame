using UnityEngine;
using UnityEngine.UI;

public class CastleHUDController : MonoBehaviour {

    public Slider wayProgress;
    public Text timer;

    public GameObject[] icons;

    private void OnEnable() {
        Initialization();
    }

    private void Initialization() {
        SetWayStatus(0f);
        SetTime(0f);
        foreach (var item in icons) {
            item.SetActive(false);
        }
    }

    public void SetTime(float time) {
        float minutes = (int)time / 60;
        float seconds = (int)time % 60;
        timer.text = minutes + " : " + (seconds < 10 ? "0" : "") + seconds;
    }

    public void SetWayStatus(float value) {
        wayProgress.value = value;
    }

    public void SetPlayerIcon(int player) {
        if (player < icons.Length) {
            icons[player].SetActive(enabled);
        } else {
            if (icons.Length > 0) icons[0].SetActive(true);
        }
    }
}
