using System.Collections;
using UnityEngine;

public class FinishTrigger : MonoBehaviour {
    public GameObject disable;
    public GameObject enable;

    private bool _isCollision = false;
    private Coroutine _rotateCoroutine = null;

    private void OnTriggerEnter(Collider other) {
        if (!_isCollision && other.gameObject.layer == LayerMask.NameToLayer("Player")) {
            StartCoroutine(Boom());
        }
    }

    private IEnumerator Boom() {
        var player = CastleLevelController.Instance.player;
        Transform playerT = player.transform;
        Animator animator = player.animator;
        Rigidbody rigidbody = player.RigidBody;
        rigidbody.isKinematic = true;
        player.isCameraMove = false;

        OffSearchlightTrigger();

        Rotate(playerT.forward, Vector3.left, 0.25f, playerT);

        Camera cameraSettings = Camera.main;
        Transform cameraT = cameraSettings.transform;
        float oldFieldOfView = cameraSettings.fieldOfView;


        for (float t = 0f; t < 4f; t += Time.deltaTime) {
            playerT.position += player.speed * Time.deltaTime * Vector3.left;
            //camera.forward = Vector3.Lerp(currentDirection, needDirection, t / cameraApproximationTime);
            cameraSettings.fieldOfView = Mathf.Lerp(oldFieldOfView, oldFieldOfView + 15f, t / 4f);
            cameraT.Rotate(new Vector3(-20f / (4f / Time.deltaTime), 0f, 0f));
            yield return null;
        }
        rigidbody.velocity = Vector3.zero;
        animator.SetTrigger("Idle");
        Rotate(playerT.forward, Vector3.right, 0.25f, playerT);
        for (float t = 0f; t < 1f; t += Time.deltaTime) { yield return null; }

        CastleBoom();

        for (float t = 0f; t < 3f; t += Time.deltaTime) { yield return null; }
        CastleLevelController.Instance.EndGame(true);
    }

    private void OffSearchlightTrigger() {
        var searchlights = FindObjectsOfType<SearchlightTrigger>(true);
        foreach (var item in searchlights) {
            item._isCollision = true;
        }
    }

    private void CastleBoom() {
        disable.SetActive(false);
        enable.SetActive(true);
    }

    public void Rotate(Vector3 from, Vector3 to, float time, Transform player) {
        if (from == to) return;
        float angle = SignedAngleBetween(from, to, Vector3.up);
        if (_rotateCoroutine != null) StopCoroutine(_rotateCoroutine);
        _rotateCoroutine = StartCoroutine(RotatePlayer(angle, time, player)); ;
    }

    private IEnumerator RotatePlayer(float angle, float time, Transform player) {
        for (float t = 0f; t < time; t += Time.deltaTime) {
            float delta = angle / (time / Time.deltaTime);
            player.Rotate(new Vector3(0, delta, 0));
            yield return null;
        }
    }

    private float SignedAngleBetween(Vector3 a, Vector3 b, Vector3 n) {
        // angle in [0,180]
        float angle = Vector3.Angle(a, b);
        float sign = Mathf.Sign(Vector3.Dot(n, Vector3.Cross(a, b)));

        // angle in [-179,180]
        float signedAngle = angle * sign;

        // angle in [0,360] (not used but included here for completeness)
        //float angle360 =  (signed_angle + 180) % 360;

        return signedAngle;
    }
}
