using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CastleLevelController : MonoBehaviour {
    public PlayerController player;
    public Transform start;
    public Transform finish;

    private float _startX;
    private float _finishX;
    private Transform _playerT;

    private float _time = 0f;
    private CastleHUDController _hud;

    private static CastleLevelController _instance = null;
    public static CastleLevelController Instance {
        get {
            if (_instance == null) {
                _instance = GameObject.FindObjectOfType<CastleLevelController>();
                if (_instance == null) {
                    Debug.LogError("Can not find level controller (CastleLevelController) :(");
                }
            }
            return _instance;
        }
    }

    private void Start() {
#if UNITY_EDITOR
        InterfaceController.Instance.InitilizateHUDWindow(1);
#endif
        StartGame();
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.F12)) {
            EndGame(true);
        }
    }

    public void StartGame() {
        Initializate();
        player.IsMoving = true;
        StartCoroutine(StartCount());
    }

    private void Initializate() {
        _hud = InterfaceController.Instance.CurrentHUD.GetComponent<CastleHUDController>();
        _startX = start.position.x;
        _finishX = finish.position.x;
        _playerT = player.transform;
        _hud.SetPlayerIcon(player.player);
    }

    public void StopCount() {
        StopAllCoroutines();
    }

    public void EndGame(bool isWin) {
        StopAllCoroutines();
        if (isWin) {
            if (Progress.game.levelCompleted[Progress.game.StartLevel] == false) {
                Progress.game.levelCompleted[Progress.game.StartLevel] = true;
                Progress.game.CurrentState = Progress.game.StartLevel + 2;
            }
            InterfaceController.Instance.ShowWinWindow(_time);
        } else {
            InterfaceController.Instance.ShowLoseWindow(_time);
        }
        player.IsMoving = false;
    }

    private IEnumerator StartCount() {
        while (true) {
            _hud.SetWayStatus(PlayerWayProgress());
            _hud.SetTime(_time += Time.deltaTime);
            yield return null; ;
        }
    }

    private float PlayerWayProgress() {
        return Mathf.Clamp01(Mathf.Max(0f, _playerT.position.x - _startX) / (_finishX - _startX));
    }
}
