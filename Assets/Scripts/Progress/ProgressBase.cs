﻿namespace Jam.Progress
{
    using UnityEngine;
    using System;
    using System.Reflection;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml;
    using System.Xml.Serialization;

 
    [Serializable]
    public class ProgressBase<T> : MonoBehaviour where T : ProgressBase<T>
    {

        #region Singleton instance

        private static T p_instance;
        protected static T instance
        {
            get
            {
                if (p_instance == null)
                {
                    GameObject go = new GameObject("_progress");
                    p_instance = go.AddComponent<T>();
                    DontDestroyOnLoad(go);
                    LoadAllFields(p_instance);
                }
                return p_instance;
            }
        }

        public static T GetInstance()
        {
            return instance;
        }

        #endregion

        #region Private Auto Methods
        void OnApplicationQuit()
        {
            Save();
            PlayerPrefs.Save();
        }
        void OnApplicationPause(bool pauseStatus)
        {
            Save();
        }
        #endregion

        #region Private Methods (Load/Save/Clear)

        static private void LoadAllFields(T _instance)
        {
            BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic;
            FieldInfo[] fields = _instance.GetType().GetFields(flags);
            foreach (FieldInfo fInfo in fields)
            {
                if (fInfo.FieldType.IsArray)
                    LoadArrayField(fInfo);
                else
                    LoadField(fInfo);
            }
        }

        static private void LoadField(FieldInfo fieldInfo)
        {
            string key = fieldInfo.FieldType.ToString() + "+" + fieldInfo.Name;
            if (PlayerPrefs.HasKey(key))
            {
                var obj = Deserialize(PlayerPrefs.GetString(key), fieldInfo.FieldType);
                fieldInfo.SetValue(instance, obj);
            }
            else
            {
                CreateDefaultField(fieldInfo);
            }
        }

        static private void LoadArrayField(FieldInfo fieldInfo)
        {
            string key = fieldInfo.FieldType.ToString() + "+" + fieldInfo.Name;
            if (PlayerPrefs.HasKey(key))
            {
                Array obj = Deserialize(PlayerPrefs.GetString(key), fieldInfo.FieldType) as Array;
                Array array = fieldInfo.GetValue(instance) as Array;
                CreateDefaultField(fieldInfo, Mathf.Max(obj.Length, array.Length));
                for (int i = obj.GetLowerBound(0); i <= Mathf.Min(obj.GetUpperBound(0), array.GetUpperBound(0)); i++)
                {
                    array.SetValue(obj.GetValue(i), i);
                }
                fieldInfo.SetValue(instance, array);
            }
            else
                CreateDefaultField(fieldInfo);
        }

        static private void CreateDefaultField(FieldInfo fieldInfo, int arrayLength = -1)
        {
            if (fieldInfo.FieldType.IsArray)
            {
                if (arrayLength < 0)
                    arrayLength = (fieldInfo.GetValue(instance) as Array).Length;
                Array array = Array.CreateInstance(fieldInfo.FieldType.GetElementType(), arrayLength);
                for (int i = array.GetLowerBound(0); i <= array.GetUpperBound(0); i++)
                    array.SetValue(Activator.CreateInstance(fieldInfo.FieldType.GetElementType()), i);
                fieldInfo.SetValue(instance, array);
            }
            else
                fieldInfo.SetValue(instance, Activator.CreateInstance(fieldInfo.FieldType));
        }

        protected static void SaveField(string fieldName)
        {
            BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic;
            FieldInfo[] fields = instance.GetType().GetFields(flags);
            foreach (FieldInfo fInfo in fields)
            {
                if (fInfo.Name == fieldName)
                {
                    instance.SaveField(fInfo);
                    return;
                }
            }
        }

        protected void SaveField(FieldInfo fieldInfo)
        {
            string key = fieldInfo.FieldType.ToString() + "+" + fieldInfo.Name;
            string value = Serialize(fieldInfo.GetValue(instance), fieldInfo.FieldType);
            PlayerPrefs.SetString(key, value);
        }

        protected void ClearAllFields()
        {
            BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic;
            FieldInfo[] fields = instance.GetType().GetFields(flags);
            foreach (FieldInfo fInfo in fields)
                CreateDefaultField(fInfo, fInfo.FieldType.IsArray ? (fInfo.GetValue(instance) as Array).Length : -1);
        }
#endregion

#region Public Methods Save
		public static void Save<_type>()
        {
            BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic;
            FieldInfo[] fields = instance.GetType().GetFields(flags);
            foreach (FieldInfo fInfo in fields)
            {
                if (fInfo.FieldType == typeof(_type))
                    instance.SaveField(fInfo);
            }
        }

		public static void Save()
        {
            BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic;
            FieldInfo[] fields = instance.GetType().GetFields(flags);
            foreach (FieldInfo fInfo in fields)
                instance.SaveField(fInfo);
        }
#endregion

#region XML Helpers
        private static string Serialize(object details)
        {
            return Serialize(details, details.GetType());
        }

        private static string Serialize<TT>(object details)
        {
            return Serialize(details, typeof(TT));
        }

        private static string Serialize(object details, System.Type type)
        {
            XmlSerializer serializer = new XmlSerializer(type);
            MemoryStream stream = new MemoryStream();
            serializer.Serialize(stream, details);
            StreamReader reader = new StreamReader(stream);
            stream.Position = 0;
            string retSrt = reader.ReadToEnd();
            stream.Flush();
            stream = null;
            reader = null;
            return retSrt;
        }

        private static TT Deserialize<TT>(string details)
        {
            return (TT)Deserialize(details, typeof(TT));
        }

        private static object Deserialize(string details, System.Type type)
        {
            XmlSerializer serializer = new XmlSerializer(type);
            XmlReader reader = XmlReader.Create(new StringReader(details));
            return serializer.Deserialize(reader);
        }
#endregion
    }

}