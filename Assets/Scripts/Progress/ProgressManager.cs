﻿using UnityEngine;
using System;


[Serializable]
public class Progress : Jam.Progress.ProgressBase<Progress>
{
    [Serializable]
    public class Game
    {
        public int CurrentState = 1;
        public bool goToSelectLevels = false;
        public bool[] levelCompleted = new bool[6];
        public bool[] ShowSujet = new bool[6];
        public bool StartNewGame = false;
        public int StartLevel = 0;
    }
    [Serializable]
    public class Levels
    {
      
    }

    [Serializable]
    public class Settings
    {
      
    }

   
    #region Private Vars
    [SerializeField]
    private Game p_game = new Game();

    [SerializeField]
    private Levels p_levels = new Levels();

    [SerializeField]
    private Settings p_settings = new Settings();
    #endregion

    #region Public Vars

    public static Game game
    {
        get { return instance.p_game; }
        set { instance.p_game = value; SaveField("p_game"); }
    }
    public static Levels levels
    {
        get { return instance.p_levels; }
        set { instance.p_levels = value; SaveField("p_levels"); }
    }
    public static Settings settings
    {
        get { return instance.p_settings; }
        set { instance.p_settings = value; SaveField("p_settings"); }
    }

    public static void Reset()
    {
        PlayerPrefs.Save();
        GetInstance().ClearAllFields();
    }
    #endregion
}