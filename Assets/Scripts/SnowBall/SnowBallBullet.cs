using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody), typeof(LineRenderer))]
public class SnowBallBullet : MonoBehaviour
{
    public Vector3 initalVelocity;
    public float damage = 50f;
    public float forcePower = 2f;
    public float coefScale = 0.1f;
    public LayerMask layersForEnemy;

    internal bool isPlayerBullet = false;
    internal SnowBallPlayer Gunster;

    private LineRenderer _lineRenderer;
    internal Rigidbody _rb;
    private Vector3 _vectorStart;
    private Vector3 _vectorCurrent;


    private int _maxIterations = 10000;
    private int _maxSegmentCount = 300;
    private float _segmentStepModulo = 0.5f;

    private Vector3[] _segments;
    private int _numSegments = 0;
    private Vector3 _startScale;
    private GameObject _arrow;

    
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _lineRenderer = GetComponent<LineRenderer>();
    }
    private void Update()
    {
         if(_rb.isKinematic)
        {
            this.transform.position = Gunster.pointSpawnShowBall.position;
        }
    }

    public void MouseDown(bool isEnemy = false)
    {
        _rb.isKinematic = true;
        if (isEnemy == false)
        {
            _arrow = PoolSnowBall.Instance.GetObjWhithName(PoolSnowBall.Names.Curva_prafab);
            _arrow.transform.position = this.transform.position;
            _startScale = _arrow.transform.localScale;
            // vectorStart = Camera.main.ScreenToWorldPoint(Input.mousePosition + new Vector3(0, 0, 5f));
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit raycastHit;
            if (Physics.Raycast(ray, out raycastHit))
            {
                _vectorStart = raycastHit.point;
                _vectorCurrent = _vectorStart;
            }
            Gunster.isMoved = false;
            _vectorStart = new Vector3(_vectorStart.x, _vectorStart.y + 3, _vectorStart.z);
        }
    }
    public void MouseDrag()
    {
        // vectorCurrent = Camera.main.ScreenToWorldPoint(Input.mousePosition + new Vector3(0, 0, 5f));
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit raycastHit;
        if (Physics.Raycast(ray, out raycastHit, float.MaxValue, LayerMask.GetMask("Default")))
        {
           _vectorCurrent = raycastHit.point;
        }
        
        initalVelocity = (_vectorStart - _vectorCurrent) * forcePower;
        initalVelocity = new Vector3(initalVelocity.x, initalVelocity.y, initalVelocity.z);
        SimulatePath(this.gameObject, initalVelocity, _rb.mass, _rb.drag);
    }
    public void MouseUp(Vector3 _initalVelocity, bool isEnemy = false)
    {
        if (isEnemy == false)
        {
            _rb.velocity = initalVelocity;
            _arrow.transform.localScale = _startScale;
            _arrow.SetActive(false);
            _arrow = null;
        }
        else
        {
            _rb.velocity = _initalVelocity;
        }
        _rb.isKinematic = false;
        Gunster.Reload();
        Gunster.isMoved = true;
    }
    private void OnCollisionEnter(Collision collision)
    {
        _lineRenderer.positionCount = 0;
        ContactPoint contact = collision.contacts[0];
        GameObject objContact = contact.otherCollider.gameObject;
        if (objContact.CompareTag("Player") || objContact.CompareTag("Enemy"))
        {
            SnowBallPlayer snowBallPlayer;
            objContact.TryGetComponent<SnowBallPlayer>(out snowBallPlayer);
            if (snowBallPlayer != null)
            {
                if (snowBallPlayer.hp > 0)
                    snowBallPlayer.hp -= damage;
                if (snowBallPlayer.hp <= 0)
                {
                    snowBallPlayer.death = true;
                    if(snowBallPlayer.CROV != null)
                       snowBallPlayer.CROV.SetActive(true);
                }
            }
        }
        _rb.isKinematic = true;
        this.gameObject.SetActive(false);

    }

    public void SimulatePath(GameObject gameObject, Vector3 forceDirection, float mass, float drag)
    {
        Rigidbody rigidbody = gameObject.GetComponent<Rigidbody>();

        float timestep = Time.fixedDeltaTime;

        float stepDrag = 1 - drag * timestep;
        Vector3 velocity = forceDirection / mass/1.2f * timestep;
        Vector3 gravity = Physics.gravity * timestep * timestep;
        Vector3 position = gameObject.transform.position + /*Vector3.up *2 +*/ rigidbody.centerOfMass;

        if (_segments == null || _segments.Length != _maxSegmentCount)
        {
            _segments = new Vector3[_maxSegmentCount];
        }

        _segments[0] = position;
        _numSegments = 1;

        for (int i = 0; i < _maxIterations && _numSegments < _maxSegmentCount && position.y > 0f; i++)
        {
            velocity += gravity;
            velocity *= stepDrag;

            position += velocity;

            if (i % _segmentStepModulo == 0)
            {
                _segments[_numSegments] = position;
                _numSegments++;
            }
        }
        _segments[_numSegments - 1] = new Vector3(_segments[_numSegments - 1].x, _segments[_numSegments - 1].y + 10, _segments[_numSegments - 1].z);
        if (_arrow == null)
        {
            _arrow = PoolSnowBall.Instance.GetObjWhithName(PoolSnowBall.Names.Curva_prafab);
            _arrow.transform.position = this.transform.position;
        }
        if (_arrow != null && _arrow.activeSelf == false)
            _arrow.SetActive(true);
        var tempQuater = Quaternion.LookRotation(_segments[_numSegments - 1] - _arrow.transform.position);
        tempEuler = tempQuater.eulerAngles;
        _arrow.transform.eulerAngles = tempEuler;// + Vector3.left * coofangle;// Quaternion.Slerp(_arrow.transform.rotation, Quaternion.LookRotation(segments[numSegments] - _arrow.transform.position), 100 * Time.deltaTime);
        Draw();
    }
    private Vector3 tempEuler;
    //public float coofangle = 12f;
    private void Draw()
    {

        ///line render
       /* _lineRenderer.transform.position = _segments[0];
        _lineRenderer.positionCount = _numSegments;
        for (int i = 0; i < _numSegments; i++)
        {
            _lineRenderer.SetPosition(i, _segments[i]);
        }*/
        //mesh
        float dist = Vector3.Distance(_segments[0], _segments[_numSegments - 1]);
        _arrow.transform.localScale = _startScale * dist * coefScale;
        if (_arrow.transform.localScale.x > 1.6)
        {
            _arrow.transform.localScale = Vector3.one * 1.6f;
        }
        _arrow.transform.localScale = new Vector3(_startScale.x, _arrow.transform.localScale.y, _arrow.transform.localScale.z/ 1.2f);


    }
    private void OnDisable()
    {
        if (this.gameObject.activeSelf == false)
            if(_arrow != null)
                _arrow.SetActive(false);
    }

}
