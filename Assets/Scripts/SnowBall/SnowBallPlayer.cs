using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SnowBallPlayer : MonoBehaviour
{
    public bool isPlayer = false;
    public bool isFrendlyEnemy = false;
    public GameObject CROV;
    public Transform pointSpawnShowBall;
    [Header("Bullet")]
    public PoolSnowBall.Names bullet;
    [Header("Stats")]
    public float hp;
    public float timeReload = 2f;
    public bool  needMove = true;
    public float speedMove = 0.1f;
    public bool useZ = false;
    public float minZ = -5;
    public float maxZ = 5;
    
    public bool useX = false;
    public float minX = -5;
    public float maxX = 5;
    public Sprite spriteIco;

    public Animator anim;

    public SpriteRenderer sprite;
    public Action act;
    internal float _maxHP;
    internal bool isMoved = true;
    internal bool canShoot = false;
    internal bool death = false;
    internal bool deathAnim = false;
    private int _directionMove = -1;
    private SnowBallBullet bulletScr;
    private SnowBallPlayer _target;
    private bool UmovaFor3Lvl = false;
    private void OnEnable()
    {
        Spawn(true);
        NewTarget();
        _maxHP = hp;
        death = false;
        anim.SetTrigger("Walk");

    }
    void NewTarget()
    {
       
        if (isPlayer == false && isFrendlyEnemy == false)
        {
            List<SnowBallPlayer> players = new List<SnowBallPlayer>();
            var temp = GameObject.FindObjectsOfType<SnowBallPlayer>();
            foreach (var i in temp)
            {
                if (i.isPlayer == true || i.isFrendlyEnemy == true)
                {
                    players.Add(i);
                }
            }
            int temps = 0;
            foreach (var i in players)
            {
                if (i.name == "DEATH2" && i.death == true)
                    temps++;
                if (i.name == "DEATH1" && i.death == true)
                    temps++;
            }
            if (temps >= 2)
                UmovaFor3Lvl = true;
            if (SceneManager.GetActiveScene().name == "Snow_koval_03" && UmovaFor3Lvl == false)
            {
                if (players.Count > 0)
                {
                    foreach (var i in players)
                    {
                        if(i.name == "DEATH2" && i.death == false)
                            _target = i;
                        if (i.name == "DEATH1" && i.death == false)
                            _target = i;
                    }
                    
                }
            }
            else
            {
              
                if (players.Count > 0)
                {
                    int rand = UnityEngine.Random.Range(0, players.Count);
                    _target = players[rand];
                }
            }
        }
        if (isPlayer == false && isFrendlyEnemy == true)
        {
            List<SnowBallPlayer> Enemy = new List<SnowBallPlayer>();
            var temp = GameObject.FindObjectsOfType<SnowBallPlayer>();

            foreach (var i in temp)
            {
                if (i.isPlayer == false && i.isFrendlyEnemy == false)
                {
                    Enemy.Add(i);
                }
            }
            if (Enemy.Count > 0)
            {
                int rand = UnityEngine.Random.Range(0, Enemy.Count);
                _target = Enemy[rand];
            }
        }
    }
    private void OnDisable()
    {
        if(bulletScr)
        bulletScr.gameObject.SetActive(false);
    }
    public void Spawn(bool needDeley = false)
    {
        GameObject _bulletObj = PoolSnowBall.Instance.GetObjWhithName(bullet);
        _bulletObj.transform.position = pointSpawnShowBall.position;
        bulletScr = _bulletObj.GetComponent<SnowBallBullet>();
        bulletScr.Gunster = this;
        if(bulletScr._rb != null)
            bulletScr._rb.isKinematic = true;
        if (needDeley)
        {
            if (isPlayer)
                canShoot = true;
            else
                StartCoroutine(Delay());
        }else
                canShoot = true;
        if (isPlayer)
            isMoved = true;
    }
    IEnumerator Delay()
    {
        yield return new WaitForSeconds(timeReload);
        canShoot = true;
    }
    public void Reload()
    {
        StartCoroutine(ReloadTime());
    }
    IEnumerator ReloadTime()
    {
            isMoved = true;
        if (death == false && isPlayer == false)
            anim.SetTrigger("Walk");
            yield return new WaitForSeconds(timeReload);
            isMoved = false;
        if (death == false)
            anim.SetTrigger("SnowballMakeCrouch");
            yield return new WaitForSeconds(1);
        if (death == false)
            anim.SetTrigger("SnowballMakeStand");
            yield return new WaitForSeconds(1);
        if (death == false && isPlayer == true)
            anim.SetTrigger("Idle");

        if (death == false)
                Spawn();
    }
    private void FixedUpdate()
    {
        if (death)
        {
            if (deathAnim == false)
            {
                if (anim.GetCurrentAnimatorStateInfo(0).IsName("Dead") == false)
                    anim.SetTrigger("Dead");
                deathAnim = true;
            }
            sprite.size = new Vector2(0, 0.3f);
            return;
        }
        if (isPlayer == false && isMoved && needMove)
        {
            if (useZ)
            {
                transform.position = transform.position + Vector3.forward * _directionMove * speedMove;
                if (transform.position.z >= maxZ)
                {
                    _directionMove = -1;
                }
                else if (transform.position.z <= minZ)
                {
                    _directionMove = 1;
                }
            }
            if (useX)
            {
                transform.position = transform.position + Vector3.right * _directionMove * speedMove;
                if (transform.position.x >= maxX)
                {
                    _directionMove = -1;
                }
                else if (transform.position.x <= minX)
                {
                    _directionMove = 1;
                }
            }
        }
        else if (isPlayer == true && isMoved && needMove)
        {
            if (useZ)
            {
                if (transform.position.z < maxZ && transform.position.z > minZ)
                {
                    
                    if (Input.GetKey(KeyCode.W))
                    {

                        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Walk") == false)
                            anim.SetTrigger("Walk");
                        transform.position = transform.position + Vector3.forward * speedMove;
                    }
                    if (Input.GetKey(KeyCode.S))
                    {
                        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Walk") == false)
                            anim.SetTrigger("Walk");
                        transform.position = transform.position + Vector3.forward * -1 * speedMove;
                    }
                    if (Input.anyKey == false)// Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S))
                    {
                        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Idle") == false)
                            anim.SetTrigger("Idle");
                    }
                }
                if (transform.position.z >= maxZ)
                {
                    transform.position = new Vector3(transform.position.x, transform.position.y, maxZ - 0.01f);
                }
                if (transform.position.z <= minZ)
                {
                    transform.position = new Vector3(transform.position.x, transform.position.y, minZ + 0.01f);
                }
            }

            if (useX)
            {
                if (transform.position.x < maxX && transform.position.x > minX)
                {
                  
                    if (Input.GetKey(KeyCode.W))
                    {
                        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Walk") == false)
                            anim.SetTrigger("Walk");
                        transform.position = transform.position + Vector3.right * speedMove;
                    }
                    if (Input.GetKey(KeyCode.S))
                    {
                        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Walk") == false)
                            anim.SetTrigger("Walk");
                        transform.position = transform.position + Vector3.right * -1 * speedMove;
                    }
                    if (Input.anyKey == false)// if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S))
                    {
                        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Idle") == false)
                            anim.SetTrigger("Idle");
                    }
                }
                if (transform.position.x >= maxX)
                {
                    transform.position = new Vector3(maxX - 0.01f, transform.position.y, transform.position.z);
                }
                if (transform.position.x <= minX)
                {
                    transform.position = new Vector3(minX + 0.01f, transform.position.y, transform.position.z);
                }
            }
        }

        if (isPlayer == false)
        {
            if (canShoot)
            {
                if (_target != null)
                {
                    if (_target.death == true)
                        NewTarget();
                    if (_target.death == false)
                    {
                        if (isFrendlyEnemy)
                            act?.Invoke();
                        StartCoroutine(ShotEnemy(_target.transform.position));
                        canShoot = false;
                        Reload();
                    }
                }else NewTarget();
            }
        }
        sprite.size =new Vector2((hp / _maxHP) * 3f , 0.3f);
    }
    public float tempSpeed;
    IEnumerator ShotEnemy(Vector3 posPlayerStart)
    {
        anim.SetTrigger("ThrowSnowball");
        float t = 0;
            bulletScr._rb.isKinematic = true;
        while (t < 1.5)
        {
            yield return 0;
           
            if (bulletScr.gameObject.activeSelf)
                bulletScr.transform.position = Parabola(bulletScr.transform.position, posPlayerStart, 3, t / 1.5f);
            if (t > 1.4)
            {
                bulletScr._rb.isKinematic = false;
                bulletScr._rb.velocity = (new Vector3(1, -1, 1) * tempSpeed);
            }
            t += Time.deltaTime;
        }
            yield return 0;
        if (bulletScr.gameObject.activeSelf)
        {
            
        }
    }
    public static Vector3 Parabola(Vector3 start, Vector3 end, float height, float t)
    {
        Func<float, float> f = x => -4 * height * x * x + 4 * height * x;
        var mid = Vector3.Lerp(start, end, t);
        return new Vector3(mid.x, f(t) + Mathf.Lerp(start.y, end.y, t), mid.z);
    }
    public void OnMouseDown()
    {
        if (isPlayer)
        {
            if (canShoot)
                bulletScr.MouseDown(!isPlayer);
        }
    }
    private void OnMouseDrag()
    {
        if (isPlayer)
        {
            if (canShoot)
            bulletScr.MouseDrag();
        }
    }
    private void OnMouseUp()
    {
        if (isPlayer)
        {
            act?.Invoke();
            if (canShoot)
            {
                anim.SetTrigger("ThrowSnowball");
                bulletScr.MouseUp(Vector3.zero, !isPlayer);
            }
            canShoot = false;
        }
    }

}

