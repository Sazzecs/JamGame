using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolSnowBall : MonoBehaviour
{
    private static PoolSnowBall _instance = null;
    public static PoolSnowBall Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<PoolSnowBall>();
                if (_instance == null)
                {
                    Debug.LogError("POOL mast ADD");
                }
            }
            return _instance;
        }
    }
    [System.Serializable]
    public class ObjectForPool
    {
        public string names;
        public List<GameObject> objects = new List<GameObject>();
    }
    public enum Names
    {
        none,
        SnowBall1,
        SnowBall2,
        Curva_prafab
    }
    





    public GameObject GetObjWhithName(Names name)
    {
        _tGO_ToGet = Instance.GetObject(Name(name));
        _tGO_ToGet.SetActive(true);
        return _tGO_ToGet;
    }
    public static string Name(Names name)
    {
        switch (name)
        {
            case Names.none: Debug.Log("none!"); return null;
            case Names.SnowBall1: return "SnowBall1";
            case Names.SnowBall2: return "SnowBall2";
            case Names.Curva_prafab: return "Curva_prafab";
            default: Debug.Log("invalid name!"); return null;
        }
    }

    #region System Functions
    public List<ObjectForPool> List;
    private List<GameObject> _tListGO;
    private GameObject _tGO;
    private int _tCount;
    private static GameObject _tGO_ToGet;
    public int GetIndexByName(string name)
    {
        int index = -1;
        _tCount = List.Count;
        for (int i = 0; i < _tCount; i++)
        {
            if (List[i].names == name)
            {
                index = i;
                break;
            }
        }

        return index;
    }
    public GameObject GetObject(string name)
    {
        int index = GetIndexByName(name);

        if (index == -1)
        {
            Debug.LogError($"GetObject | Wrong NAME! {name}");
            return null;
        }



        _tListGO = List[index].objects;
        _tCount = _tListGO.Count;
        for (int i = 0; i < _tCount; i++)
        {
            _tGO = _tListGO[i];
            try
            {
                if (!_tGO.activeSelf)
                {
                    return _tGO;
                }
            }
            catch
            {
                Debug.LogError("Not Exist: " + name);
            }
        }

#if UNITY_EDITOR
        Debug.Log($"GetObject | Not Enough in list! name {name}");
#endif

        return Add(_tListGO);
    }

    public GameObject Add(List<GameObject> list)
    {
        GameObject g = Instantiate(list[0], list[0].transform.parent);
        list.Add(g);
        return g;
    }
    public void DisableAll()
    {
        int count = List.Count;
        int count2 = 0;
        ObjectForPool objList;
        GameObject tGO;
        for (int i = 0; i < count; i++)
        {
            objList = List[i];
            count2 = objList.objects.Count;
            for (int j = 0; j < count2; j++)
            {
                tGO = objList.objects[j];
                if (tGO != null)
                {
                    tGO.SetActive(false);
                }
            }
        }
    }
    #endregion
}
