using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InterfaceController : MonoBehaviour {
    [Header("Menu pause")]
    [SerializeField] private GameObject _menuPauseWindow;
    [SerializeField] private Button _bContinue;
    [SerializeField] private Button _bSettins;
    [SerializeField] private Button _bQuit;

    [Header("Menu settings")]
    [SerializeField] private GameObject _menuSettingsWindows;
    [SerializeField] private Slider _sMusic;
    [SerializeField] private Slider _sSound;
    [SerializeField] Button _bBack;

    [Header("HUD")]
    [SerializeField] private GameObject _hudSnowBall;
    [SerializeField] private GameObject _hudCastle;

    [Header("Win windows")]
    [SerializeField] private GameObject _snowBallWinWindow;
    [SerializeField] Text _swwSnowBallNumberField;
    [SerializeField] Text _swwTimeField;
    [SerializeField] Button _bSwwNext;
    [Space(5f)]
    [SerializeField] GameObject _castleWinWindow;
    [SerializeField] Text _cwTimeField;
    [SerializeField] Button _bCwNext;

    [Header("Lose windows")]
    [SerializeField] private GameObject _snowBallLoseWindow;
    [SerializeField] Text _slwSnowBallNumberField;
    [SerializeField] Text _slwTimeField;
    [SerializeField] Button _bSlwRestart;
    [SerializeField] Button _bSLwQuit;
    [Space(5f)]
    [SerializeField] GameObject _castleLoseWindow;
    [SerializeField] Text _clTimeField;
    [SerializeField] Button _bClRestart;
    [SerializeField] Button _bClQuit;

    [Header("HINTS")]
    [SerializeField] GameObject _hint1;
    [SerializeField] GameObject _hint2;
    [SerializeField] GameObject _hint3;
    [SerializeField] GameObject _hint4;
    [SerializeField] GameObject _hint5;
    [SerializeField] GameObject _hint6;
    [SerializeField] Button _hint1B;
    [SerializeField] Button _hint2B;
    [SerializeField] Button _hint3B;
    [SerializeField] Button _hint4B;
    [SerializeField] Button _hint5B;
    [SerializeField] Button _hint6B;
    private Window _window;
    private GameObject _currentHUD;

    public GameObject CurrentHUD {
        get => _currentHUD;
    }


    private static InterfaceController _instance = null;
    public static InterfaceController Instance {
        get {
            if (_instance == null) {
                _instance = GameObject.FindObjectOfType<InterfaceController>();
                if (_instance == null) {
                    Debug.LogError("This level has not interface :(");
                }
            }
            return _instance;
        }
    }

    private enum Window {
        hud,
        pause,
        settings,
        finish
    }

    public enum ETypeLevel {
        snowBall,
        castle
    }

    private void Awake() {
        Initialization();
    }

    private void Update() {
        if (Input.GetKeyUp(KeyCode.Escape)) {
            switch (_window) {
                case Window.hud:
                    Pause();
                    break;
                case Window.pause:
                    ContinueGame();
                    break;
                case Window.settings:
                    BackToMenuPause();
                    break;
                default:
                    break;
            }
        }
    }

    private void Pause() {
        Time.timeScale = 0f;
        _currentHUD.SetActive(false);
        _menuPauseWindow.SetActive(true);
        _window = Window.pause;
    }

    private void Initialization() {
        Time.timeScale = 1f;
        InitilizateHUDWindow();
        InitialzateMenuPauseWindow();
        InitializateMenuSettingsWindow();
        InitialzateFinishWindows();
        _window = Window.hud;
    }

    private void InitilizateHUDWindow()
    {
        _hudSnowBall.SetActive(false);
        _hudCastle.SetActive(false);
        int levelId = Progress.game.StartLevel;
        _currentHUD = (levelId % 2) switch
        {
            0 => _hudSnowBall,
            _ => _hudCastle
        };
        _currentHUD.SetActive(true);
        if (Progress.game.ShowSujet[levelId] == false)
        {
            Progress.game.ShowSujet[levelId] = true;
            Time.timeScale = 0;
            switch (levelId)
            {
                case 0: _hint1.gameObject.SetActive(true); break;
                case 1: _hint2.gameObject.SetActive(true); break;
                case 2: _hint3.gameObject.SetActive(true); break;
                case 3: _hint4.gameObject.SetActive(true); break;
                case 4: _hint5.gameObject.SetActive(true); break;
                case 5: _hint6.gameObject.SetActive(true); break;
            }
        }
        _hint1B.onClick.AddListener(hideAllHint);
        _hint2B.onClick.AddListener(hideAllHint);
        _hint3B.onClick.AddListener(hideAllHint);
        _hint4B.onClick.AddListener(hideAllHint);
        _hint5B.onClick.AddListener(hideAllHint);
        _hint6B.onClick.AddListener(hideAllHint);
    }
    void hideAllHint()
    {
        Time.timeScale = 1;
        _hint1.gameObject.SetActive(false);
        _hint2.gameObject.SetActive(false);
        _hint3.gameObject.SetActive(false);
        _hint4.gameObject.SetActive(false);
        _hint5.gameObject.SetActive(false);
        _hint6.gameObject.SetActive(false);
    }
    public void InitilizateHUDWindow(int levelId) {
        _hudSnowBall.SetActive(false);
        _hudCastle.SetActive(false);
        _currentHUD = (levelId % 2) switch {
            0 => _hudSnowBall,
            _ => _hudCastle
        };
        _currentHUD.SetActive(true);
        if (Progress.game.ShowSujet[levelId] == false)
        {
            Progress.game.ShowSujet[levelId] = true;
            Time.timeScale = 0;
            switch (levelId)
            {
                case 0: _hint1.gameObject.SetActive(true); break;
                case 1: _hint2.gameObject.SetActive(true); break;
                case 2: _hint3.gameObject.SetActive(true); break;
                case 3: _hint4.gameObject.SetActive(true); break;
                case 4: _hint5.gameObject.SetActive(true); break;
                case 5: _hint6.gameObject.SetActive(true); break;
            }
        }
        _hint1B.onClick.AddListener(hideAllHint);
        _hint2B.onClick.AddListener(hideAllHint);
        _hint3B.onClick.AddListener(hideAllHint);
        _hint4B.onClick.AddListener(hideAllHint);
        _hint5B.onClick.AddListener(hideAllHint);
        _hint6B.onClick.AddListener(hideAllHint);
    }

    // Menu pause
    private void InitialzateMenuPauseWindow() {
        _bContinue.onClick.AddListener(ContinueGame);
        _bSettins.onClick.AddListener(OpenSettings);
        _bQuit.onClick.AddListener(ReturnToMainMenu);

        _bContinue.gameObject.SetActive(true);
        _bSettins.gameObject.SetActive(true);
        _bQuit.gameObject.SetActive(true);

        _menuPauseWindow.SetActive(false);
    }

    private void ContinueGame() {
        _menuPauseWindow.SetActive(false);
        _currentHUD.SetActive(true);
        Time.timeScale = 1;
        _window = Window.hud;
    }

    private void OpenSettings() {
        _menuPauseWindow.SetActive(false);
        _menuSettingsWindows.SetActive(true);
        _window = Window.settings;
    }

    private void ReturnToMainMenu() {
        Progress.game.goToSelectLevels = true;
        SceneManager.LoadScene("MainMenu");
    }

    // Menu settings
    private void InitializateMenuSettingsWindow() {
        _sMusic.onValueChanged.AddListener(ChangeMusicValue);
        _sSound.onValueChanged.AddListener(ChangeSoundValue);
        _bBack.onClick.AddListener(BackToMenuPause);

        _sMusic.gameObject.SetActive(true);
        _sSound.gameObject.SetActive(true);
        _bBack.gameObject.SetActive(true);

        _menuSettingsWindows.SetActive(false);
    }

    private void ChangeMusicValue(float value) {

    }

    private void ChangeSoundValue(float value) {

    }

    private void BackToMenuPause() {
        _menuSettingsWindows.SetActive(false);
        _menuPauseWindow.SetActive(true);
        _window = Window.pause;
    }

    // Finish windows
    public void InitialzateFinishWindows() {
        _snowBallWinWindow.SetActive(false);
        _castleWinWindow.SetActive(false);
        _snowBallLoseWindow.SetActive(false);
        _castleLoseWindow.SetActive(false);

        _bSwwNext.onClick.AddListener(NextLevel);
        _bCwNext.onClick.AddListener(NextLevel);

        _bSlwRestart.onClick.AddListener(RestartLevel);
        _bSLwQuit.onClick.AddListener(QuitToMenu);
        _bClRestart.onClick.AddListener(RestartLevel);
        _bClQuit.onClick.AddListener(QuitToMenu);
    }

    private void NextLevel() {
        // TODO: ������������ �������� �����
        Progress.game.goToSelectLevels = true;
        SceneManager.LoadScene("MainMenu");
    }

    private void RestartLevel() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void QuitToMenu() {
        ReturnToMainMenu();
    }

    public void ShowWinWindow(int snowBallNumber, float time) {
        //Time.timeScale = 0f;
        _currentHUD.SetActive(false);
        _snowBallWinWindow.SetActive(true);
        _swwSnowBallNumberField.text = snowBallNumber.ToString();
        float minutes = (int)time / 60;
        float seconds = (int)time % 60;
        _swwTimeField.text = minutes + " : " + (seconds < 10 ? "0" : "") + seconds;
    }

    public void ShowWinWindow(float time) {
        //Time.timeScale = 0f;
        _currentHUD.SetActive(false);
        _castleWinWindow.SetActive(true);
        float minutes = (int)time / 60;
        float seconds = (int)time % 60;
        _cwTimeField.text = minutes + " : " + (seconds < 10 ? "0" : "") + seconds;
    }

    public void ShowLoseWindow(int snowBallNumber, float time) {
        //Time.timeScale = 0f;
        _currentHUD.SetActive(false);
        _snowBallLoseWindow.SetActive(true);
        _slwSnowBallNumberField.text = snowBallNumber.ToString();
        float minutes = (int)time / 60;
        float seconds = (int)time % 60;
        _slwTimeField.text = minutes + " : " + (seconds < 10 ? "0" : "") + seconds;
    }

    public void ShowLoseWindow(float time) {
        //Time.timeScale = 0f;
        _currentHUD.SetActive(false);
        _castleLoseWindow.SetActive(true);
        float minutes = (int)time / 60;
        float seconds = (int)time % 60;
        _clTimeField.text = minutes + " : " + (seconds < 10 ? "0" : "") + seconds;
    }
}
