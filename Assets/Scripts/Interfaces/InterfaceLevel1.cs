using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InterfaceLevel1 : MonoBehaviour
{
    [System.Serializable]
    public class enemy
        {
        public GameObject obj;
        public Image ico;
        public Image icoDeath;
    }
    public Slider sliderPower;
    public List<enemy> objEnemy;
    public List<enemy> objFrendly;
    List<SnowBallPlayer> players = new List<SnowBallPlayer>();
    List<SnowBallPlayer> enemys = new List<SnowBallPlayer>();
    SnowBallPlayer player;
    [Header("Lose")]
    public GameObject loseObj;
    public Text snijokKinuto;
    public Text TimerProvedeno;
    public Button btnMenu;
    public Button btnRestart;
    [Header("Win")]
    public GameObject winObj;
    public Text snijokKinutoW;
    public Text TimerProvedenoW;
    public Button btnMenuW;

    private float _time = 0f;
    private int _counterSnijok = 0;
    void Act()
    {
        _counterSnijok++;
    }
    private void Awake()
    {
        btnMenuW.onClick.RemoveAllListeners();
        btnRestart.onClick.RemoveAllListeners();
        btnMenu.onClick.RemoveAllListeners();

        btnMenuW.onClick.AddListener(Menu);
        btnMenu.onClick.AddListener(Menu);
        btnRestart.onClick.AddListener(Restart);

        loseObj.SetActive(false);
        winObj.SetActive(false);
        Time.timeScale = 1;
        _time = 0;
        var temp = GameObject.FindObjectsOfType<SnowBallPlayer>();

        foreach (var i in temp)
        {
            if (i.isPlayer == true || i.isFrendlyEnemy == true)
            {
                players.Add(i);
                i.act = Act;
                if (i.isPlayer == true || i.isFrendlyEnemy == false)
                    player = i;
            }
            if (i.isPlayer == false && i.isFrendlyEnemy == false)
            {
                enemys.Add(i);
            }
            
        }
        foreach (var i in objEnemy)
        {
            i.obj.SetActive(false);
        }
        foreach (var i in objFrendly)
        {
            i.obj.SetActive(false);
        }
        for (int i = 0; i < players.Count; i++)
        {
            objFrendly[i].obj.SetActive(true);
            objFrendly[i].ico.sprite = players[i].spriteIco; 
        }
        for (int i = 0; i < enemys.Count; i++)
        {
            objEnemy[i].obj.SetActive(true);
            objEnemy[i].ico.sprite = enemys[i].spriteIco;
        }
    }
    private void Update()
    {

        if (Input.GetKeyDown(KeyCode.F12))
        {
            foreach (var i in enemys)
            {
                i.death = true;
            }
        }
        _time += Time.deltaTime;
        float allHP = 0;
        float hpEnemy = 0; 
        float hpPlayers = 0;
        for (int i = 0; i < players.Count; i++)
        {
            allHP += players[i].hp;
            hpPlayers += players[i].hp;
            objFrendly[i].icoDeath.gameObject.SetActive(players[i].death);
        }
        for (int i = 0; i < enemys.Count; i++)
        {
            allHP += enemys[i].hp;
            hpEnemy += enemys[i].hp;
            objEnemy[i].icoDeath.gameObject.SetActive(enemys[i].death);
        }
        sliderPower.value = hpEnemy / allHP;
        bool win = true;
        bool lose = true;
        foreach (var i in enemys)
        {
            if (i.death == false)
                win = false;
        }
        /*foreach (var i in players)
        {
            if (i.death == false)
                lose = false;
        }*/
        if (player.death == false)
            lose = false;
        if (win)
        {
            winObj.SetActive(true);
            Time.timeScale = 0;
            if (Progress.game.levelCompleted[Progress.game.StartLevel] == false)
            {
                Progress.game.levelCompleted[Progress.game.StartLevel] = true;
                Progress.game.CurrentState = Progress.game.StartLevel + 2;
            }
            TimerProvedenoW.text = (System.Math.Round((_time / 60f), 2)).ToString();
            snijokKinutoW.text = (_counterSnijok).ToString();
        }
        if (lose)
        {
            loseObj.SetActive(true);
            Time.timeScale = 0;
            TimerProvedeno.text = (System.Math.Round((_time / 60f), 2)).ToString();
            snijokKinuto.text = (_counterSnijok).ToString();
        }
    }
    void Menu()
    {
        Time.timeScale = 1;
        Progress.game.goToSelectLevels = true;
        SceneManager.LoadScene("MainMenu");
    }
    void Restart()
    {
        var curScene = SceneManager.GetActiveScene();
        Time.timeScale = 1;
        SceneManager.LoadScene(curScene.name);
    }
}
