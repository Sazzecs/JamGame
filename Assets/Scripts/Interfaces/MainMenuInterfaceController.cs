using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuInterfaceController : MonoBehaviour {

    [Header("Dop Quit")]
    [SerializeField] private GameObject _menuQuitWindow;
    [SerializeField] private Button _bQuitDQ;
    [SerializeField] private Button _bQuitBack;

    [Header("Menu Main")]
    [SerializeField] private GameObject _menuMainWindow;
    [SerializeField] private Button _bContinue;
    [SerializeField] private Button _bNewGame;
    [SerializeField] private Button _bSettings;
    [SerializeField] private Button _bQuit;

    [Header("Menu Settings")]
    [SerializeField] private GameObject _menuSettingsWindow;
    [SerializeField] private Button _bBack;

    [Header("Menu Levels")]
    [SerializeField] private GameObject _menuLevelsWindow;
    [SerializeField] private Button _bBacklevel;
    [SerializeField] private List<LevelButton> levelButtons = new List<LevelButton>();

    [System.Serializable]
    public class LevelButton
    {
        public GameObject loc;
        public GameObject complete;
        public Button button;
        internal int id;
    }


    private void Awake() {
        Time.timeScale = 1;
        Initialization();
    }

    private void Initialization() {
        InitializateMenuMainWindows();
        InitializateMenuSettingsWindow();
        InitializateMenuLevelsWindow();
        if (Progress.game.goToSelectLevels)
        {
            Progress.game.goToSelectLevels = false;
            ContinueGame();
        }
    }

    private void InitializateMenuMainWindows() {
        _bContinue.onClick.AddListener(ContinueGame);
        _bNewGame.onClick.AddListener(StartNewGame);
        _bSettings.onClick.AddListener(OpenSettings);
        _bQuit.onClick.AddListener(QuitGame);

        _bContinue.gameObject.SetActive(CheckSaveGame());
        _bNewGame.gameObject.SetActive(true);
        _bSettings.gameObject.SetActive(true);
        _bQuit.gameObject.SetActive(true);

        _bQuitDQ.onClick.AddListener(QuitGameForce);
        _bQuitBack.onClick.AddListener(BackToMain);

        _menuQuitWindow.gameObject.SetActive(false);
        _menuMainWindow.SetActive(true);
    }

    // Menu main
    private bool CheckSaveGame() {
        return Progress.game.StartNewGame;
    }

    private void ContinueGame() {
        _menuLevelsWindow.gameObject.SetActive(true);
        _menuSettingsWindow.SetActive(false);
        _menuMainWindow.SetActive(false);
    }

    private void StartNewGame()
    {
       // Progress.game.StartLevel = 0;
        Progress.game.StartNewGame = true;
        ContinueGame();
       //SceneManager.LoadScene("Snow_koval");
    }

    private void OpenSettings() {
        _menuLevelsWindow.SetActive(false);
        _menuMainWindow.SetActive(false);
        _menuSettingsWindow.SetActive(true);
    }

    private void QuitGame() {
        if (Progress.game.levelCompleted[Progress.game.levelCompleted.Length - 1] == true)
        {

#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
        }
        else {
            _menuQuitWindow.gameObject.SetActive(true);
        }
    }
    private void QuitGameForce()
    {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
    // Menu settings
    private void InitializateMenuSettingsWindow() {
       
        _bBack.onClick.AddListener(BackToMain);

        _bBack.gameObject.SetActive(true);

        _menuSettingsWindow.SetActive(false);
    }

    private void ChangeMucicVolume(float value) {

    }

    private void ChangeSoundVolume(float value) {

    }

    private void BackToMain() {
        _menuSettingsWindow.SetActive(false);
        _menuMainWindow.SetActive(true);
        _menuLevelsWindow.SetActive(false);
        _menuQuitWindow.SetActive(false);
    }
    // Menu levels
    private void InitializateMenuLevelsWindow()
    {
        _bBacklevel.onClick.AddListener(BackToMain);
        _bBacklevel.gameObject.SetActive(true);
        _menuLevelsWindow.SetActive(false);

        levelButtons[0].complete.SetActive(Progress.game.levelCompleted[0]);
        levelButtons[0].loc.SetActive(false);
        levelButtons[0].id = 1;

        for (int i = 1;  i< levelButtons.Count; i++)
        {
            levelButtons[i].complete.SetActive(Progress.game.levelCompleted[i]);
            levelButtons[i].loc.SetActive(!Progress.game.levelCompleted[i-1]);
            levelButtons[i].id = i+1;
        }
        foreach (var i in levelButtons)
        {
            i.button.onClick.RemoveAllListeners();
            i.button.onClick.AddListener(() => { loadLevel(i.id-1); });
        }
    }
    void loadLevel(int id)
    {
        if (levelButtons[id].loc.activeSelf)
            return;
        Debug.Log(id);
        Progress.game.StartLevel = id;
        switch (id)
        {
            case 0: SceneManager.LoadScene("Snow_koval_01"); break;
            case 1: SceneManager.LoadScene("lvl3_1"); break;
            case 2: SceneManager.LoadScene("Snow_koval_02"); break;
            case 3: SceneManager.LoadScene("lvl3_2"); break;
            case 4: SceneManager.LoadScene("Snow_koval_03"); break;
            case 5: SceneManager.LoadScene("lvl3_3"); break;
        }
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            BackToMain();
        }
    }
}
