using UnityEngine;

public interface IWinAnimation {
    public void PlayWinAnimation(Rigidbody mainObject, Animator animator);
}
