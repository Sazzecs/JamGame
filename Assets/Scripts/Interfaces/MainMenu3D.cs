using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu3D : MonoBehaviour
{
    public GameObject First;
    public GameObject Second;
    public GameObject Finish;
    private void OnEnable()
    {
        First.SetActive(false);
        Second.SetActive(false);
        Finish.SetActive(false);
        switch (Progress.game.CurrentState)
        {
            case 1:
                First.SetActive(true);
                break;
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                Second.SetActive(true);
                break;
            case 7:
                Finish.SetActive(true);
                break;
        }
    }
}
