using System.Collections;
using UnityEngine;

public class CastlePlayerWinWithDynamite : MonoBehaviour, IWinAnimation {

    public void PlayWinAnimation(Rigidbody mainObject, Animator animator) {
        StartCoroutine(SetDynamite(mainObject, animator));
    }

    private IEnumerator SetDynamite(Rigidbody mainObject, Animator animator) {
        int childs = mainObject.transform.childCount;
        for (int i = 0; i < childs; i++) {
            mainObject.transform.GetChild(i).gameObject.SetActive(false);
        }
        mainObject.isKinematic = false;
        animator.SetTrigger("SnowballMakeCrouch");
        for (float t = 0f; t < 1f; t += Time.deltaTime) { yield return null; }
        for (int i = 0; i < childs; i++) {
            mainObject.transform.GetChild(i).gameObject.SetActive(true);
        }
        animator.SetTrigger("Idle");
        for (float t = 0f; t < 1f; t += Time.deltaTime) { yield return null; }
        CastleLevelController.Instance.EndGame(true);
    }

}
